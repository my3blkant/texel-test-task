export default function DefaultLayout({ children }: any) {
    return (<div>

        <nav className="navbar navbar-light bg-light">
            <div className="container-fluid">
                <a className="navbar-brand" href="#">Navbar</a>
            </div>
        </nav>
        <div className="container">
            {children}
        </div>
    </div>
    )
}