import { v4 as uuidv4 } from 'uuid'
import Redis from './redis';


export type TaskState = {
    status: "pending" | "error" | "completed",
    data?: string,
};

export function getStatus(task_uuid: string): Promise<TaskState> {
    return Redis.get(`task:${task_uuid}`).then(async value => {
        if (value === null) {
            // task doesn't exist
            return Promise.reject('Not exists')
        }
        let data;
        if (value === "error") {
            data = await Redis.get(`task:${task_uuid}:error`)
        }
        if (value === "completed") {
            data = await Redis.get(`task:${task_uuid}:data`)
        }
        return { status: value, data } as TaskState;
    })
}

export async function createTask(runner: () => Promise<string>): Promise<string> {
    let task_uuid = uuidv4();
    let task_key = Redis.set(`task:${task_uuid}`, "pending")
    task_key.then(async ok => {
        if (ok) {
            try {

                let result = await runner();
                await Redis.set(`task:${task_uuid}:data`, result)
                await Redis.set(`task:${task_uuid}`, "completed")
            } catch (err) {
                await Redis.set(`task:${task_uuid}:error`, err)
                await Redis.set(`task:${task_uuid}`, "error")
            }
        }
    })

    await task_key
    return task_uuid;
}
