import fs from 'fs/promises';
import path from 'path'

import Redis from './redis'

let config_path = path.resolve(path.join('config', 'service.json'));


export interface ConfigFile {
    name: string;
    title: string;
    description: string;
    parameters: Parameters;
    commands: Commands;
}


export interface Commands {
    start: string;
}


export interface Parameters {
    input: Put[];
    output: Put[];
}


export interface Put {
    type: string;
    name: string;
    title: string;
    items?: Item[];
}


export interface Item {
    value: string;
    title: string;
}


const CONFIG_KEY = 'config:file';
const CONFIG_RETAIN_SEC = 20;

function readConfig(): Promise<ConfigFile> {
    return fs.readFile(config_path).then(data => {
        let j = JSON.parse(data.toString());
        return j as ConfigFile;
    })
}

function cacheConfig(config: ConfigFile): Promise<ConfigFile> {
    return Redis.setex(CONFIG_KEY, CONFIG_RETAIN_SEC, JSON.stringify(config))
        .then(() => config)
        .catch(() => config)
}

export function getConfig(): Promise<ConfigFile> {
    return Redis.get(CONFIG_KEY)
        .then(result => {
            if (result) {
                console.log("Cache hit");
                return JSON.parse(result || "") as ConfigFile
            }
            console.log("Cache miss");
            return readConfig()
                .then(cacheConfig)

        })
        .catch(err => {
            console.log("Redis error");
            return readConfig()
        })

}