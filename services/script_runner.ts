import { spawn } from 'child_process';
import path from 'path';

export type Params = {
    [key: string]: string;
}

let scripts_dir = path.resolve('scripts');

function mapParams(args: string[], params: Params): string[] {

    console.log(`args ${args}`);
    console.log(`params ${JSON.stringify(params)}`);
    return args.map(arg => {
        console.log(`param ${arg}`);
        if (arg in params) {
            return params[arg]
        }
        return arg;

    })
}

export function runScript(cmd: string, params: Params) :Promise<string>{
    return new Promise((res, rej) => {

        let data = ''
        let err = ''

        console.log(`Script dir: ${scripts_dir}`)

        let args = cmd.split(' ');
        let script_name = args[0];
        args = mapParams(args.slice(1), params);

        let proc = spawn(script_name, args, {
            cwd: scripts_dir,
        });

        proc.stdout.on('data', d => {
            data += d.toString();
        })
        proc.stderr.on('data', d => {
            err += d
        })
        proc.on('close', code => {
            if (code !== 0) {
                return rej(err)
            }
            setTimeout(()=> res(data), 4000);
            // return res(data)
        })
        proc.on('error', (err) => {
            return rej(`Can't spawn process, ${err}`)
        })
    })
}