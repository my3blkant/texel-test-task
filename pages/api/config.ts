import type { NextApiRequest, NextApiResponse } from 'next'

import { getConfig, ConfigFile } from '../../services/config';

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<ConfigFile>
) {
    return getConfig()
        .then(data => res.status(200).json(data))
        .catch(err => res.status(400).json(err));
}
