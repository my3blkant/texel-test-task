import type { NextApiRequest, NextApiResponse } from 'next'

import { getConfig, ConfigFile, Put } from '../../services/config';
import { createTask } from '../../services/jobs';
import { Params, runScript } from '../../services/script_runner';


function validate(params: Put[], key: string, val: any) {
    let item = params.find(p => p.name === key);
    if (item) {
        if (item.type === 'number') {
            if (typeof val === "string" && !isNaN(parseInt(val))) return true;
            return false;
        }
        if (item.type === 'select') {
            console.log(`validate ${JSON.stringify(item)} ${key} ${val}`)
            let opt = item.items?.find(i => i.value === val);
            console.log(`val ${opt}`)
            if (opt) return true;
            return false;
        }
    }
    return false;
}

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<any>
) {
    if (req.method !== 'POST') {
        return res.status(405).send('Method not allowed');
    }

    getConfig()
        .then(async config => {
            console.log(`query ${JSON.stringify(req.query)}`)

            let script = config.commands.start;
            let params = {} as any;
            for (let arg in req.body) {
                if (validate(config.parameters.input, arg, req.body[arg])) {
                    params[`\${${arg}}`] = req.body[arg];
                } else {
                    return Promise.reject(`Validation error: ${arg}`);
                }
            }
            let task_id = await createTask(() => runScript(script, params as Params))
            return task_id;

        })
        // .then(data => {
        //     return new Promise(res => setTimeout(() => res(data), 1000));
        // })
        .then(data => res.status(200).json({ data }))
        .catch(err => res.status(400).json({ message: err }));
}
