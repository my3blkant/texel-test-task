import type { NextApiRequest, NextApiResponse } from 'next'

import { getStatus, TaskState } from '../../../services/jobs';

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<TaskState>
) {
    return getStatus(req.query.uuid as string)
        .then(data => res.status(200).json(data))
        .catch(err => res.status(400).json(err));
}
